<!DOCTYPE html>
<html>
<body>
	<h1>Buat Account Baru!</h1>
	<h2>Sign Up Form</h2>

	<form action="welcome">
	  <p><label for="firstname">First name:</label></p>
	  <p><input type="text" id="firstname" name="firstname" required></p>
	  <p><label for="lastname">Last name:</label></p>
	  <p><input type="text" id="lastname" name="lastname" required></p>
	  <p><label for="gender">Gender:</label></p>
	  <p><input type="radio" id="male" name="gender" value="male">
		<label for="male">Male</label><br>
		<input type="radio" id="female" name="gender" value="female">
		<label for="female">Female</label><br>
		<input type="radio" id="other" name="gender" value="other">
		<label for="other">Other</label></p>

	  <p><label for="nationality">Nationality:</label></p>
	  <p><select name="cars" id="cars">
			<option value="indonesian">Indonesian</option>
			<option value="singaporean">Singaporean</option>
			<option value="malaysian">Malaysian</option>
			<option value="australian">Australian</option>
		  </select></p>

	  <p><label for="language">Language Spoken:</label></p>
	  <p><input type="checkbox" id="langbahasaindonesia" name="language" value="langbahasaindonesia">
		<label for="langbahasaindonesia">Bahasa Indonesia</label><br>
		<input type="checkbox" id="langenglish" name="language" value="langenglish">
		<label for="english">English</label><br>
		<input type="checkbox" id="langother" name="language" value="langother">
		<label for="other">Other</label></p>

       <p><label for="bio">Bio:</label></p>
       <p><textarea id="bio" name="bio" rows="10" cols="35">
		  </textarea></p>

	  <p><input type="submit" value="Sign Up"></p>
	</form> 
</body>
</html>
